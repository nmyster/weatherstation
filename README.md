# Weather Station
A series of libraries that can be used to manage, analyse, control and interact with AWS.
### Supported:
The point of Weather Station is to provide a single source that has the same functionality on multiple platforms/languages. The currently supported platforms are:

* Python 
* Ruby

### Future functionality
A feature to Weather Station will only be made live if it is implemented fully in at least two supported platforms. At this time, Python and Ruby are the two MVP languages but I would like to add NodeJS and other support once there is a little more flesh to Python & Ruby
# Usage
Usage of each language should be the same. Their may be differences in usage depending on the language being used, For a full list of features/functionality for each language, check the specific README in each sub directory
##Python
```pip install -r requirements.txt```

##Ruby
``` gem install weatherstation```


