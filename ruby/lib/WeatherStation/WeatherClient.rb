require 'aws-sdk'

module WeatherStation
    class WeatherClient

        attr_accessor :config, :aws, :conn

        def initialize(profile: nil, region: nil, iamprofile: nil)
            """Inital Method for WeatherClient."""
            puts "Welcome to Cloudreach WeatherStation"
            
            @config = {}
            @config['profile'] = profile
            @config['region'] = region
            @config['iamprofile'] = iamprofile
            @aws = WeatherConnect.new(profile: profile, region: region, iamprofile: iamprofile)
            @conn = nil

        end


        def connect()
            """Method to connect to aws - returns true if valid connection."""
            return true if @aws.connect()
            
        end


        def image_search(search: nil, tags: nil)
            """Method to search for AMI's with specified search."""
            if connect then
                puts "Searching images with the %s" % [search]
                e = @aws.connection('ec2')
                f = get_filter(s: search, t: tags)

                return e.describe_images({
                    owners: ["self"],
                    filters: f
                }).images
            end

        end

        def security_group_search(search: nil, tags: nil)
            """Security group search based on search term."""
            if connect then
                puts "Searching for security groups with %s" % [search]

                f = get_filter(s: search, t: tags)

                return @aws.connection('ec2').describe_security_groups({
                    filters: f
                }).security_groups

            end

        end


        def get_filter(s: nil, t: nil)
            """Return an array of filters based on search term and tags being provided."""
            f = []
             if t then
                t.each do | t |
                    f << {
                            name: 'tag:%s' % [t[0]],
                            values:t[1]
                        }
                end
            end

            if s then
                f << {
                    name: 'tag:Name',
                    values: [s]
                }
            end

            return f
        end


        def instance_search(search: nil, tags: nil)
            """Instance search based on search term or tag array."""
            if connect then
                puts "Searching for instances with %s" % [search]
            
                f = get_filter(s: search, t: tags)

                return @aws.connection('ec2').describe_instances({
                        filters: f
                        }).reservations.map{|x| x[:instances][0]}

            end
        end

        def process_tags(tags: nil)
            """Return hash with keys as tag keys and values and tag values"""
            # Create empty tags hash to return
            out = {}

            # For each of the tags, add a key/value pair to out based on key/value of each tag
            tags.each do | t |
                out[t.key] = t.value
            end  

            # Return tags
            return out

        end

        def confirm(message=nil)
            puts "here"
            if message then
                message = "%s (Yes/y or No/n)" % [message]
                puts message
                answer = gets.chomp().downcase()
                if answer == "yes" || answer == "y" then
                    puts "Doing it!"
                    return true
                else
                    puts "Not doing it!"
                    return false
                end
            else
                return false
            end

        end


        def image_clean(images: nil, snapshots: true)
            """Deregister images provided and clear associated snapshots if required."""
            unless images then
                puts "no images"
            else
                puts "Cleaning..."

                # for each image
                #   load image resource for that image
                #       load image snapshots
                #       delete image
                #       loop through snapshots
                #           load snapshot as resource
                #            delete snapshot
                #
            end
        end

    end
end

# w = WeatherClient.new(profile: "lab", region: 'eu-west-1')

# w.connect()

# i = w.aws.connection('ec2', resource: true)

# puts i.class

# # images = w.image_search(tags: {"BackupRetention" => ["*15*"]})
# # images.each do | image | 
# #     puts "%s has a name of %s " % [image.image_id,image.name]
# # end

# # puts "\n"

# # sgs = w.security_group_search(tags:{'Name' => ['*zenoss*']})
# # sgs.each do | sg |
# #     puts w.process_tags(tags: sg.tags)
# #     puts "%s has a name of %s" % [sg.group_id,sg.group_name]
# # end

# # puts "\n"

# # ec2 = w.instance_search(tags:{'Owner' => ["*seb*"]})
# # ec2.each do | l |
# #     t = w.process_tags(tags: l.tags)
# #     puts "%s has a name of %s and was created on %s and is owned by %s" % [l.instance_id,t['Name'],l.launch_time,t['Owner']]
# # end

