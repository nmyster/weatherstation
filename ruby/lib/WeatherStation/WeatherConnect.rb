module WeatherStation

    class WeatherConnect

        attr_accessor :config, :session, :clients

        def initialize(profile: nil, region: nil, iamprofile: false)

            @config = {}
            @config['profile'] = profile
            @config['region'] = region
            @config['iamprofile'] = iamprofile
            @session = nil
            @clients = {'resources' => {}}
            @connected = false

        end

        def connect()
            unless @connected then
                @session = Aws::SharedCredentials.new(profile_name: @config['profile']) unless @session
                @connected = true
            end

            return @session
        end

        def connection(service, resource: false)
            if connect then
                if !resource then
                    if @clients.has_key?(service) then
                        puts "Client for %s already created. Using that" % [service]
                        return @clients[service]            
                    else
                        case service 
                            when 'ec2'
                                @clients[service] = ''
                                 @clients[service] = Aws::EC2::Client.new(
                                    region: @config['region'],
                                    credentials: @session
                                )
                        end
                        return @clients[service]
                    end
                else
                    if !@clients['resources'].has_key?(service) then
                        case service
                            when 'ec2'
                                @clients['resources'][service] = ''
                                @clients['resources'][service] = Aws::EC2::Resource.new(
                                    client: self.connection('ec2')
                                )
                        end
                    end

                    return @clients['resources'][service]

                end
            end
        end

    end
end