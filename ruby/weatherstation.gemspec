lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |s|
  s.name        = 'weatherstation'
  s.version     = '0.0.1'
  s.date        = '2016-04-17'
  s.summary     = "Tools for AWS work"
  s.description = "A set of libs to use in AWS projects or for simple interactions"
  s.authors     = ["Neil Stewart"]
  s.email       = 'nmyster@gmail.com'
  s.files       =  `git ls-files`.split($/)
  s.homepage    = ''
  s.license       = 'MIT'

end