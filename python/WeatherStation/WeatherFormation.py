"""
WeatherFormation Class.

To create Troposphere templates with added functionality.
"""

from botocore.exceptions import ClientError
import logging
import os
import time
import sys
import yaml
import json
from troposphere import Template, Tags
from troposphere import Base64, Join
from troposphere.autoscaling import Tag


class WeatherFormation(Template):
    """Docstring for WeatherFormation."""

    def __init__(self, profile=None, region=None, connector=None):
        """Docstring for WeatherFormation."""
        super(Template, self).__init__()
        self.project_path = os.path.dirname(os.path.realpath(__file__))
        self.description = None
        self.metadata = {}
        self.conditions = {}
        self.mappings = {}
        self.outputs = {}
        self.parameters = {}
        self.resources = {}
        self.version = None
        self.aws = connector
        self.upload_config = {}
        self.stack_config = {'stackname': None, 'exists': False}
        self.log = self.logging()

    def logging(self):
        """Logging method."""
        logging.getLogger(__name__)
        return logging

    def set_upload(
            self,
            bucket=None,
            objectname=None,
            local=None,
            localpath=None):
        """Set bucket and object details for uploading."""
        self.upload_config['bucket'] = bucket
        self.upload_config['object'] = objectname
        self.upload_config['url'] = 'https://s3.amazonaws.com/{}\
/{}'.format(bucket, objectname)
        self.upload_config['local'] = local
        self.upload_config['localpath'] = localpath

        return True

    def load_user_data(self, file=None, params=None, whitespace=False):
        """
        Import userdata from a file.

        This function ignore blank lines within the file.
        Special characters are automatically escaped.

        Args:
            filepath (string): The absolute path to the file
            containing the userdata to be imported.
            params (dict): contains dict of parameters to
            substitute in data using str Formatter class

        Returns:
            Base64 object: The Base64 object being passed a Join object
            with strings.
            If file not found, an empty string list is used.
        """
        data = []
        try:

            with open(file, 'r') as f:
                for line in f:
                    data.append(line)
        except IOError:
            raise IOError('Error opening or reading file: ' + file)

        # If params are provided, try to load them into user data
        if params:
            try:
                data = ''.join(data).format(**params)
            except KeyError as e:
                raise KeyError('Key {} is expected but not defined'.format(e))
            return Base64(data)

        else:
            return Base64(Join('', data))

    def upload(self, bucket=None, objectname=None):
        """Upload the template to s3."""
        if bucket:
            self.upload_config['bucket'] = bucket

        if objectname:
            self.upload_config['object'] = objectname

        self.log.info('Uploading template to s3://{}/{}'.format(
            self.upload_config['bucket'],
            self.upload_config['object']))

        if self.upload_to_s3():
            return self

    def stack_exists(self):
        """
        Check that the stackname.

        self.stack_config['stackname'] exists.
        """
        if self.stack_config['stackname']:

            if not self.stack_config['exists']:
                stacks = self.aws.connection(
                    'cloudformation').describe_stacks()['Stacks']

                # From the stacks, get the one we want
                sub_stack = [x for x in stacks if x['StackName'] ==
                             self.stack_config['stackname']]

                if len(sub_stack) == 1:
                    self.log.info("Stack {} exists".format(
                        self.stack_config['stackname']))
                    self.stack_config['exists'] = True
                    return True
                else:
                    self.log.info("Stack {} does not exist".format(
                        self.stack_config['stackname']))
                    return False
            else:
                return True

        else:
            raise Exception("No stackname set")

    def confirm(self, message):
        """Method to confirm an action."""
        # Check python persion
        if sys.version_info >= (3, 0):
            a = input("{} (yes/y or no/n): ".format(message)).lower()
        else:
            a = raw_input("{} (yes/y or no/n): ".format(message)).lower()

        if a == "y" or a == "yes":
            return True
        else:
            return False

    def apply(self, stackname=None):
        """Apply template to AWS."""
        if self.confirm("Please confirm you wish to apply this template"):
            self.log.info("applying")
            print "applying"
            if stackname:
                self.stackname = stackname
                print stackname
            # Check stack exists
            if self.stack_exists():

                print "Updating stack"
                self.update_stack()
            else:
                self.log.info("Creating stack")
                self.create_stack()
        else:
            self.log.info("not applying")

    def waiter(self, stackname=None, delay=5):
        """Custom waiter that returns event status."""
        retry = 0
        while retry < 3:
            try:
                con = self.aws.connection('cloudformation')
                current_events = con.describe_stack_events(
                    StackName=stackname
                )['StackEvents']
                current_events.reverse()

                for event in current_events:
                        print "{} ~ {} {} | {}".format(
                            event.get('Timestamp'),
                            event.get('ResourceType'),
                            event.get('ResourceStatus'),
                            event.get('StackName')
                        )
            except Exception:
                print "Stack does not yet exist - Looking for {}".format(
                    stackname
                )
                time.sleep(1)
                retry += 1
                continue

            while True:

                try:
                    new_events = con.describe_stack_events(
                        StackName=stackname
                    )['StackEvents']
                    new_events.reverse()
                    to_show = [
                        a for a in new_events if a not in current_events
                    ]

                    current_events = new_events
                    for event in to_show:
                        print "{} ~ {} {}  Reason: {} | {}".format(
                            event.get('Timestamp'),
                            event.get('ResourceType'),
                            event.get('ResourceStatus'),
                            event.get('ResourceStatusReason'),
                            event.get('StackName')
                        )
                    status = con.describe_stacks(
                        StackName=stackname
                    )['Stacks'][0]['StackStatus']

                    if status.upper().endswith('FAILED') or status.upper().endswith('COMPLETE'):
                        print "Stack Status: {}".format(status)
                        break

                    time.sleep(5)
                except Exception:
                    print "Stack has been deleted most likely!"
                    break
            break

    def update_stack(self, stackname=None):
        """Update existing stack."""
        print "updaing"
        try:
            con = self.aws.connection('cloudformation')
            con.update_stack
            con.update_stack(
                StackName=self.stack_config['stackname'],
                TemplateURL=self.upload_config['url'],
                Capabilities=[
                    'CAPABILITY_IAM',
                ]
            )

            if self.waiter(
                stackname=self.stack_config['stackname']
            ):
                self.log.info('{} has been updated successfully'.format(
                    self.stack_config['stackname']))

        except ClientError as e:
            print e
            if "UpdateStack" in str(e):
                self.log.info("{} is already up to date".format(
                    self.stack_config['stackname']))

    def create_stack(self, stackname=None):
        """Create new stack."""
        if stackname:
            self.stack_config['stackname'] = stackname

        if not self.stack_exists():
            self.log.info('Stack does not exist, creating {}'.format(
                self.stack_config['stackname']))
            con = self.aws.connection('cloudformation')
            con.create_stack(
                StackName=self.stack_config['stackname'],
                TemplateURL=self.upload_config['url'],
                OnFailure='DELETE',
                Capabilities=[
                    'CAPABILITY_IAM',
                ]
            )
            if self.waiter(
                stackname=self.stack_config['stackname']
            ):
                self.log.info('{} has been updated successfully'.format(
                    self.stack_config['stackname']))

    def delete_stack(self, stackname=None):
        """Delete stack."""
        if stackname:
            self.stack_config['stackname'] = stackname

        if self.stack_exists():
            con = self.aws.connection('cloudformation')
            con.delete_stack(
                StackName=self.stack_config['stackname']
            )
            self.log.info("Deleting stack {}".format(
                self.stack_config['stackname']))
            self.waiter(
                stackname=self.stack_config['stackname']
            )

    def set_cloudformation(self, stackname=None):
        """
        Method to setup cloudformation.

        So we can simply apply later.
        """
        self.stack_config['stackname'] = stackname

    def load_config(self, file=None, flatten=True):
        """Load config settings into project."""
        if file:
            print self.project_path
            file = '{}/{}'.format(self.project_path, file)
            with open(file, 'r') as stream:

                try:
                    self.userconfig = yaml.load(stream)
                    return self.userconfig
                except yaml.YAMLError as e:
                    pass
                try:
                    self.userconfig = json.load(stream)
                    return self.userconfig

                except Exception as e:
                    self.log.error(e)

    def load_tags(self, tags=None, service=None):
        """Load tags for troposphere in correct format."""
        c = self.load_config(file='tag_config.yaml')['service']
        return_tags = None
        if service:
            if type(tags) is dict:
                if service in tags:
                    tags = tags[service]
            if service in c:
                tag_format = c[service]
                if tag_format != 'base_tag':
                    return_tags = []
                else:
                    return_tags = {}
            for t in tags:
                r = self.process_load_tags(t, tag_format)
                if tag_format != 'base_tag':
                    return_tags.append(r)
                else:
                    return_tags[r['Key']] = r['Value']

            if tag_format == 'base_tag':
                return_tags = Tags(**return_tags)
        else:
            return_tags = {}
            for k, tag in tags.iteritems():
                if k in c:
                    tag_format = c[k]
                    if tag_format != 'base_tag':
                        return_tags[k] = []
                    else:
                        return_tags[k] = {}
                    for t in tag:
                        r = self.process_load_tags(t, tag_format)
                        if tag_format != 'base_tag':
                            return_tags[k].append(r)
                        else:
                            return_tags[k][r['Key']] = r['Value']
                    if tag_format == 'base_tag':
                        return_tags[k] = Tags(**return_tags[k])

        return return_tags

    def process_load_tags(self, t, tag_format):
        """Process the tags to be loaded."""
        o = None
        if tag_format == 'asg_tag':
            o = Tag(t['key'], t['value'], True)
        elif tag_format == 'dict_tag':
            o = {"Key": t['key'], "Value": t['value']}
        elif tag_format == 'base_tag':
            o = {"Key": t['key'], "Value": t['value']}
        return o

    def upload_to_s3(self):
        """Hello world."""
        output = self.to_json()

        if self.upload_config['local']:
            filepath = "{}".format(
                self.upload_config['localpath'])

            if not os.path.exists(os.path.dirname(filepath)):
                try:
                    os.makedirs(os.path.dirname(filepath))
                # Guard against race condition
                except OSError as exc:
                    self.log.error(exc)

            with open(filepath, "w") as f:
                f.truncate()
                f.write(output)
                f.close()

        self.aws.connection('s3').put_object(
            Bucket=self.upload_config['bucket'],
            Body=output,
            Key=self.upload_config['object'],
            ServerSideEncryption="AES256"
        )
