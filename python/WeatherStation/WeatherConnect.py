"""
WeatherStation.

Cloudreach Toolbox for Cloud managment/interaction
"""

import boto3


class WeatherConnect(object):
    """docstring for CloudConnect."""

    def __init__(self, profile=None, region=None, iamprofile=False):
        """
        Init method for WeatherConnect.

        profile: Local IAM profile to use from ~/.aws/config
        region: AWS Region to connect to
        iamprofile: True/False - If true connection assumes using IAM role.
        """
        self.profile = profile
        self.region = region
        self.session = None
        self.clients = {'resources': {}}
        self.iamprofile = iamprofile
        self.connected = False

    def connect(self):
        """Connect method."""
        if not self.connected:
            if not self.iamprofile:
                self.session = boto3.session.Session(
                    profile_name=self.profile,
                    region_name=self.region)
            else:
                self.session = boto3

            self.connected = True
        return self.connected

    def connection(self, service, resource=False):
        """
        Connection method.

        Return a usable AWS connection to the specified service.
        """
        if self.connect():
            if not resource:
                if service not in self.clients:
                    self.clients[service] = self.session.client(service)

                return self.clients[service]
            else:
                if service not in self.clients['resources']:
                    if service == 'ec2':
                        self.clients[
                            'resources'
                        ][service] = self.session.resource(service)

                return self.clients['resources'][service]
