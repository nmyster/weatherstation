"""
WeatherStation.

Cloudreach Toolbox for Cloud managment/interaction
"""

from .WeatherConnect import WeatherConnect
from .WeatherFormation import WeatherFormation
from botocore.exceptions import ClientError
import yaml
import json


class WeatherClient(object):
    """Main Class for weather station."""

    def __init__(self, profile=None, region=None, instanceprofile=None):
        """Init method for WeatherClient class."""
        self.profile = profile
        self.region = region
        if profile:
            self.set_profile(
                profile=profile,
                region=region,
                instanceprofile=instanceprofile
            )
        self.conn = None
        self.instanceprofile = instanceprofile
    # AMI Remover
    #  use ami searcher, for each of the results
    #   find the snapshots
    #     deregister each
    # delete if flag set (don't by default)
    # AMI Searcher
    # AMI Encrypter

    # Snapshot Cleaner
    # Snapshot Searcher

    def set_profile(self, profile=None, region=None, instanceprofile=None):
        """Set AWS profile for WeatherStation."""
        self.profile = profile
        self.region = region
        self.aws = WeatherConnect(
            profile=profile,
            region=region,
            iamprofile=instanceprofile
        )

        self.conn = None
        self.instanceprofile = instanceprofile

    def get_filter(self, t=None, s=None):
        """Get a valid filter with name and tag searches."""
        f = []
        if t:

            for tag in t:
                f.append({
                    "Name": 'tag:{}'.format(tag),
                    "Values": t[tag]
                })

        if s:
            f.append(
                {
                    "Name": "tag:Name",
                    "Values": [s]
                }
            )

        return f

    def image_search(self, search=None, tags=None):
        """AMI Search."""
        if self.connect():
            if search is None:
                search = "*"

            e = self.aws.connection(service='ec2')
            f = self.get_filter(s=search, t=tags)

            images = e.describe_images(
                Filters=f

            )['Images']

            if len(images) > 0:
                return images[-1]
            else:
                return "no image pal"

    def security_group_search(self, search=None, tags=None):
        """Security group search based on search terms."""
        if self.connect():
            if search is None:
                search = "*"
            f = self.get_filter(t=tags, s=search)

            return self.aws.connection('ec2').describe_security_groups(
                Filters=f
            )

    def instance_search(self, search=None, tags=None):
        """Search for instances with provided terms."""
        if self.connect():
            if search is None:
                search = "*"
            f = self.get_filter(t=tags, s=search)

            return [d['Instances'][0] for d in self.aws.connection(
                service='ec2').describe_instances(
                    Filters=f
            )['Reservations']]

    def process_tags(self, tags=None):
        """
        Process tags into a dictionary.

        Key as tag key and value as tag value.
        """
        if tags:
            out = {}
            for t in tags:
                out[t['Key']] = t['Value']

            return out

    def image_clean(self, images=None, snapshots=True):
        """Delete provided images and snapshots."""
        if not images:
            print "No Images provided"
        else:
            print "Cleaning images"

            if self.confirm(
                "Are you sure you want to delete {} image(s)".format(
                    len(images)
                )
            ):

                for image in images:
                    i = self.aws.session.resource(
                        "ec2"
                    ).Image(image['ImageId'])

                    snaps = i['Ebs']

                    i.deregister()
                    if snapshots:
                        for snap in snaps:
                            s = self.aws.connection(
                                'ec2',
                                resource=True
                            ).Snapshot(snap['SnapshotId'])

                            s.delete()
                return True

            else:
                print "Not confirmed - images not being deleted."
                return False

    def confirm(self, message=None):
        """Method to allow input confirmation from user."""
        if message:
            answer = raw_input("{} (Yes/y or No/n)".format(message)).lower()
            if answer == "y" or answer == "yes":
                print "Doing it!"
                return True
            else:
                print "Not doing it!"
                return False
        else:
            self.log.error("No message to confirm")
            return False

    def connect(self):
        """Create a connectionf rom WeatherConnect."""
        if self.aws.connect():
            return True

    def load_config(self, file=None, flatten=True):
        """Load provided configuration file."""
        c = None

        while True:
            with open(file, 'r') as stream:
                try:
                    c = yaml.load(stream)
                    break
                except yaml.YAMLError:
                    pass
                try:
                    c = json.load(stream)
                    break
                except Exception:
                    return None
                    break
        try:
            if flatten and "config" in c:
                return c['config']
            else:
                return c
        except:
            return None

    def save_config(self, data=None, file=None, unflatten=True, asyaml=True):
        """Save data to specified config file. Convert to YAML if yaml set to true."""
        backup = open(file, 'r').read()

        if 'config' not in data.keys() and unflatten:
            new_data = {}
            new_data['config'] = data
            data = new_data

        with open(file, 'w') as outfile:
            try:
                if asyaml:
                    outfile.write(yaml.dump(data, default_flow_style=False))
                else:
                    outfile.write(data)
            except Exception:
                print "File write failed, recovering!"
                with open(file, 'w') as outfile:
                    outfile.write(backup)
                raise

    def template(self,
                 bucket=None,
                 objectname=None,
                 stackname=None,
                 local=False,
                 localpath=None):
        """Create and returns a OrionCF object.

        This can then be used to build a Troposphere template
        for use with CloudFormation
        Return a new OrionCF template.
        """
        # Create new Orion object, passing details provided to this method
        wf = WeatherFormation(connector=self.aws)

        # Sets the upload details of the template for S3 and CloudFormation
        # This allows templates to be uploaded and applyed to CF through
        # single line commands
        wf.set_upload(
            bucket=bucket,
            objectname=objectname,
            local=local,
            localpath=localpath
        )
        wf.set_cloudformation(stackname=stackname)

        # Return the OrionCF object (ocf)
        return wf

    def check_function_exists(self, function):
        """Check if given lambda function exists."""
        # Create a new Lambda connection
        c = self.aws.connection('lambda')
        try:
            # Try and get the function by using the provided name
            l = c.get_function(FunctionName=function)
            return bool(l)

        except ClientError as e:
            # If we get this far, the previous command failed
            # and we return false as it does not exist
            if "ResourceNotFoundException" in str(e):
                return False

    def find_lambda_functions(self, search=None):
        """Find Lambda functions based on names in searches list."""
        # Get all lambda functions
        f = self.aws.connection(
            'lambda'
        ).list_functions()['Functions']

        # if search has been passed
        if search:
            results = []  # create results list to be used later

            # check if search is a list or string
            if type(search) is list:
                # if search is list, iterate through searches
                # to match returned functions (partial name match)
                self.log.info("search is list")
                for s in search:
                    for function in f:
                        if s in function['FunctionName']:
                            results.append(function)
            elif type(search) is str:
                # if search is string, do same as above
                # but don't iterate through searches
                self.log.info("search is string")
                for function in f:
                    if search in function['FunctionName']:
                        results.append(function)
            # return results if there is at least one function in results list
            if len(results) > 0:
                return results
            else:
                raise Exception
        # if search was not specified, return all results from initial call
        else:
            return f

    def upload_to_s3(self, file=None, bucket=None, objectname=None):
        """Upload a file to s3.

        Using the provided attributes, upload a file to S3 and
        return if successful.
        """
        # Load file
        if file:
            # ADD - Try catch to handle file read if failed/not existing
            # Read the file into f variable
            f = open(file, 'r').read()

            # Connect to S3 using OrionCF connector
            # Upload object to s3 using encryption
            r = self.aws.connection('s3').put_object(
                Bucket=bucket,
                Body=f,
                Key=objectname,
                ServerSideEncryption="AES256"
            )

            # Return true or false if response is empty or not
            # ADD - Need better error handling here - check if file
            # was successful using return data
            return bool(r)

    def update_function(
            self,
            function=None,
            code=None,
            bucketname=None,
            objectname=None):
        """Update an existing Lambda function's code.

        Uses the provided details to upload some code to S3 and then update
        the specifed lambda function to use said code
        """
        if self.upload_to_s3(
            file=code,
            bucket=bucketname,
            objectname=objectname
        ):
            # ADD - Check to see if the function exists
            # Connect to Lambda and update the function with provided details
            u = self.aws.connection('lambda').update_function_code(
                FunctionName=function,
                S3Bucket=bucketname,
                S3Key=objectname,
                Publish=True
            )

            # Returns True or false depending on response
            # ADD - Try catch around this to make it more robust
            return bool(u)
