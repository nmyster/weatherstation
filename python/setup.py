"""Setup file for the WeatherStation python module."""

from setuptools import setup, find_packages

setup(
    name='weatherstation',
    version='0.0.1',
    description='A selection of functionality to work with AWS',
    author='Neil Stewart',
    author_email='nmyster@gmail.com',
    keywords='aws cloud',
    test_suite='tests',
    package_data={'WeatherStation': ['tag_config.yaml']},
    packages=find_packages(exclude=['docs', 'tests', 'contrib']),
    install_requires=['boto3', 'pyyaml', 'mock', 'moto', 'troposphere']
)
