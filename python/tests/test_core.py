"""Core module tests."""

import unittest
from mock import Mock, patch
from WeatherStation.WeatherClient import WeatherClient


class TestWeatherCore(unittest.TestCase):
    """Core tests."""

    @patch('boto3.session.Session')
    def setUp(self, mock_boto):
        """Core test setup."""
        client = Mock()

        self.o = WeatherClient(profile='lab', region='eu-west-1')
        self.o.connect()

        mockfig = self.o.load_config(
            file='./tests/test_resources/configuration/mockfig.yaml'
        )

        client.describe_images.return_value = mockfig['images']
        client.describe_security_groups.return_value = mockfig['sgs']
        mock_boto.client.return_value = client
        self.o.aws.session = mock_boto

    def test_image_search(self):
        """Test core is loading."""
        i = self.o.image_search()
        self.assertIn("ImageId", i)
        assert i['ImageId'] == "ami-9df9s0s"

    def test_load_config_flat(self):
        """Test loading of configuration file that has been flattened."""
        c = self.o.load_config(
            file='./tests/test_resources/configuration/mockfig.yaml',
            flatten=False
        )

        self.assertIn("config", c)

    def test_load_config_fat(self):
        """Test loading of config file that has not been flattened."""
        c = self.o.load_config(
            file='./tests/test_resources/configuration/mockfig.yaml'
        )

        self.assertNotIn("config", c)

    def test_security_group_search(self):
        """Test security group search."""
        s = self.o.security_group_search()
        self.assertIn("SecurityGroups", s)
        assert s['SecurityGroups'][0]['GroupId'] == "sg-da7s8da"
