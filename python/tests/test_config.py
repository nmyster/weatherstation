"""Core module tests."""

import unittest
from WeatherStation.WeatherClient import WeatherClient


class TestWeatherConfig(unittest.TestCase):
    """Core tests."""

    def setUp(self):
        """Core test setup."""
        self.o = WeatherClient(profile='lab', region='eu-west-1')

    def test_load_config_fat(self):
        """Test loading of configuration file that has been flattened."""
        c = self.o.load_config(
            file='./tests/test_resources/configuration/mockfig.yaml',
            flatten=False
        )

        self.assertIn("config", c)

    def test_load_config_flat(self):
        """Test loading of config file that has not been flattened."""
        c = self.o.load_config(
            file='./tests/test_resources/configuration/mockfig.yaml'
        )

        self.assertNotIn("config", c)

    def test_load_yaml_config(self):
        """Test loading of yaml config file."""
        c = self.o.load_config(
            file='./tests/test_resources/test_files/config.yaml'
        )

        self.assertNotIn("config", c)

    def test_load_json_config(self):
        """Test loading of JSON config file."""
        c = self.o.load_config(
            file='./tests/test_resources/test_files/config.json'
        )

        self.assertNotIn("config", c)

    def test_fail_load_xml_config(self):
        """Test loading of XML config file."""
        c = self.o.load_config(
            file='./tests/test_resources/test_files/config.xml'
        )

        assert c is None
